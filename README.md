# leighs gift

Gift for my great friend Leigh who helped give me the skills required to make this project possible.

## How to Use
Mostly it just sits on the desk and looks pretty. There are a few ways to interact with it.

### Web Interface
The wemos mcu acts as a simple webserver. You can access this by first connecting to its broadcasted wifi network.

* SSID: flangeNet
* PASSWORD: *************

After connecting to the wifi you can access the functionality by pointing your browser to one of the following.

* flange.local
* 192.168.4.1

The following sites are available over the web interface.
#### / (root)

192.168.4.1 or flange.local

The root site is the main site and has the following options.

* screen modes
	* mode 1: name and logo with title
	* mode 2: name and logo with title and battery status
	* mode 3: name and logo
* title text
	* three lines, 8 chars max, displayed in modes 1 & 2
* leds
	* individual hue control
	* master brightness control
	* on/off checkbox
* piss off bell end
	* this will apply any changes made
* RGB scan links
	* will scan the LEDs with the color
* battery info

#### IO
192.168.4.1/IO or flange.local/IO

Accessing the IO directory will display a debug site with analog and digital read info.

#### draw
192.168.4.1/draw or flange.local/draw

Accessing the draw directory will prompt the screen to redraw. This is mostly used for debug.

### Serial Interface
The serial interface can be accessed by connecting to the TX & RX ports on the motherboard.

* 9600 baud
* 8 data bits
* 1 stop bit
* parity none
* flow control XON/XOFF

ASCII commands are sent and the command is captured when a newline '/n' character is sent.

Three commands are implemented.

* read: read data from mcu
* write: write data to mcu
* wifi: reboot the wifi AP mode (mostly for debug)

While booting the mcu will output diagnostic info over the serial connection.

The mcu will also output the webserver arguments over the serial connection.

### Power and Charging
The unit will run off of the included battery.

* Low Battery indicated by flashing red LED.
* If battery becomes critically low the unit will deep sleep until charge is applied.

In order to charge the battery when it gets low plug in a 5V usb micro lead to the port on the lipo charge board.

* Charge port is located near edge of wooden frame.
* Green LED indicates charging.
* Do not plug the charge lead into the wemos mcu port, which is also a micro usb.
* The unit can be on while charging but keep in mind if the current draw exceeds the charge current the battery will still drain.
	* This is only likely with the 8 LEDS on full brightness, and even then would be a stretch.

## Flashing New Firmware
Follow these steps in order to flash new firmware onto the mcu.

* download and install latest Arduino IDE.
	* https://www.arduino.cc/en/main/software
* once installed and running go to tools > board > boards manager...
	* in the search box search for 8266 and install the esp8266 boards package
* restart the IDE
* once running again go to sketch > include library > manage libraries... 
	* search for and install the following
		* U8g2 by oliver (2.25.10 or greater)
		* FastLED by Daniel Garcia (3.2.6 or greater)
	* the relevant wifi libraries should be installed with the boards package from the previous step
* restart the IDE
* clone the latest data from the repo
* open leighFrame.ino
	* should see the main code along with all the tabs across the top
* set all of the dip switches on the mobo to off
	* this will disconnect any external serial (tx/rx) lines as well as disconnect the D1 & Reset switch required for deep sleep
* switch the entire unit off with the main power switch
* plug in micro usb cable to the mcu board and your computer
* drivers will be installed for the CH340 usb chip
	* if not installed or failed manually install from: https://wiki.wemos.cc/downloads
* use device manager to determine what port your mcu is assigned
* in the arduino IDE go to the tools tab and set the following
	* board: LOLIN(WEMOS) D1 mini Lite
	* Upload Speed: 921600
	* CPU Frequency: 80 MHz
	* Flash Size: 1M (no SPIFFS)
	* Debug Port: Disabled
	* lwIP Variant: v2 Lower Memory
	* VTables: Flash
	* Exceptions: Disabled
	* Erase Flash: Only Sketch
	* Port: (the port your board is assigned from device manager)
* click the upload button (right arrow) or press CTRL+U
* code should compile and upload
	* errors? contact your friendly engineering department...
* once complete remove the usb cable from the mcu board
* turn the dip switches back on
* power the unit via main power switch
	
## Mechanicals
Designed in Solidworks 2017. Wooden bits made by hand, also some 3D printing.

## Electrical Hardware
Designed in Solidworks PCB 2017.

### Components
* wemos D1 mini lite
	* ESP-8285 wifi board
	* 11 digital IO
	* 1 analog input
	* 3.3V
* neopixel (WS2812) LED strip
	* individually adressable
	* 8 leds
* oled panel
	* 128x64 resolution
	* SH1106 driver
	* i2c interface
* bidirectional level shifter
	* 5v to 3.3v capabilities
* lipo charger and boost converter
	* adafruit powerboost 500c
* various LEDs, switches, battery, etc...

## Firmware
Firmware for Wemos D1 mini lite board.

### Software required
* Arduino IDE version 1.8.8
	* boards manager updated with wemos d1 mini lite board

### Libraries Required
* ESP8266WiFi
* WiFiClient
* ESP8266WebServer
* ESP8266mDNS
* FastLED
* U8g2lib
* Ticker

#### tasks remaining
* none!

#### tasks completed
* charge LED
* low battery LED
* add conditional to turn off charge glyph in mode 1 & 3
* redraw the screen when there is a change in chrg pin
* attach interrupt to charge detect pin
* add charging text to the root site
* store led hue in array so i can dynamically set the sliders this will also allow for me to scan while the leds are on
* battery remaining estimation curve (take into account load)
	* based on 0.2A discharge, which is max with all LEDs on
* prevent LEDs from turning on at startup or at least disable them after a successful boot
	* disable leds at boot (may flash during boot)
* create serial interface for reading vars
* debounce the charge input (maybe not needed...)
	* not needed
* average out the analog reads (10, maybe 100?) to prevent the voltage # and % remaing from jumping so much
	* averaging the last ten values (20 seconds total time)
* blink the charge LED when charging and go solid when at 100%
* deep sleep mode when low battery and disable all functionality check percentage every so often and wake up if above a certian percentage
* create serial interface for setting vars
	* easter egg
* LEDs not always set to proper values when repeatedly setting them or changing 
	* seems to be fairly stable now