void initPins() {
  // setup pin output/input/interrupts
  pinMode(D0, WAKEUP_PULLUP);
  pinMode(chargeLED, OUTPUT);
  pinMode(lowBatLED, OUTPUT);
  pinMode(usbPwr, INPUT);
  // init the two led pins as low (off)
  digitalWrite(chargeLED, LOW);
  digitalWrite(lowBatLED, LOW);
}

void initInterrupts() {
  // setup interrupts
  // detect charging cable plugged in interrupt
  attachInterrupt(digitalPinToInterrupt(usbPwr), chrgPinISR, CHANGE);
  // two second timer interrupt
  nonWifiTimer.attach(2, twoSecISR);
}

void initPeripherals() {
  // open serial port and start oled panel
  Serial.begin(9600);
  panel.begin();
  // init the neopixel LEDs as off
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  setAllLEDsOff();
}

void initBattery() {
  // init the battery voltage, level, and charge state
  // sleep if battery is too low
  charging = digitalRead(usbPwr);
  batteryVoltage = ((analogRead(A0) / (float)1023) * 4.2);
  if (batteryVoltage < lowSleep) {
    batDeepSleep(); 
    // does this get called before jumping to the ISR, EVERY TIME?
  }
  batteryLevel = batteryPercent(batteryVoltage);
}

void initWifi() {
  // start wifi soft AP mode and MDNS responder
  WiFi.mode(WIFI_AP);
  if (WiFi.softAP("flangeNet", "itmeansvagina")) {
    Serial.print("AP mode open. IP: ");
    Serial.println(WiFi.softAPIP());
  } else {
    Serial.println("AP mode failed");
  }
  if (MDNS.begin("flange")) {
    Serial.println("MDNS started: available at flange.local");
  } else {
    Serial.println("MDNS failed");
  }
  // production webserver directories
  server.on("/", handleRoot); // display the website when accessing the root dir (direct IP)
  server.on("/red", handleRed); // scan the leds red and return to root
  server.on("/green", handleGreen); // scan the leds green and return to root
  server.on("/blue", handleBlue); // scan the leds blue and return to root
  server.on("/all", handleAll); // scan all the leds and return to root
  // debug dirs
  server.on("/IO", handleIO); // display the website when accessing the IO dir
  server.on("/draw", handleDraw); // redraw the screen when accessing the draw dir
  // start the server
  server.begin();
  WiFi.printDiag(Serial);
}
