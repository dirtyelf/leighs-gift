void serialHandler() {
  bool readCommand = false;
  bool writeCommand = false;
  static uint8_t numReadVars = 13;
  uint8_t all = numReadVars - 1;
  unsigned long startTime;
  unsigned long timeout = 15000;
  uint8_t command;
  char inChar;
  while (Serial.available()) {
    inChar = Serial.read();
    inString += inChar;
    if (inChar == '\n') {
      Serial.println("Command Received. Parsing...");
      if (inString.startsWith("read")) {
        Serial.println("Read command. Send var to read. (12s timeout)");
        Serial.println("The following variables are available for read.\n");
        Serial.println("batVolts \t rawAnalog \t ledValues \t fastLEDs");
        Serial.println("charging \t toggles \t screenMode \t screenText");
        Serial.println("screenUpdate \t ledsUpdate \t ledsOn \t batVoltages");
        Serial.println("batUpdate \t {all}\n");
        command = 1;
      } else if (inString.startsWith("write")) {
        Serial.println("Write command.");
        command = 2;
      } else if (inString.startsWith("wifi")) {
        Serial.println("Reboot WiFi command.");
        command = 3;
      }
      else {
        command = 4;
      }
      switch (command) {
        case 1:
          inString = "";
          readCommand = true;
          startTime = millis();
          while (readCommand) {
            if (startTime + timeout < millis()) {
              Serial.println("command timeout");
              readCommand = false;
            } else {
              Serial.print(".");
              delay(250);
            }
            while (Serial.available()) {
              inChar = Serial.read();
              inString += inChar;
              if (inChar == '\n') {
                if (inString.startsWith("batVolts")) {
                  command = 0;
                } else if (inString.startsWith("ledValues")) {
                  command = 1;
                } else if (inString.startsWith("rawAnalog")) {
                  command = 2;
                } else if (inString.startsWith("fastLEDs")) {
                  command = 3;
                } else if (inString.startsWith("charging")) {
                  command = 4;
                } else if (inString.startsWith("toggles")) {
                  command = 5;
                } else if (inString.startsWith("screenMode")) {
                  command = 6;
                } else if (inString.startsWith("screenText")) {
                  command = 7;
                } else if (inString.startsWith("screenUpdate")) {
                  command = 8;
                } else if (inString.startsWith("ledsUpdate")) {
                  command = 9;
                } else if (inString.startsWith("ledsOn")) {
                  command = 10;
                } else if (inString.startsWith("batVoltages")) {
                  command = 11;
                } else if (inString.startsWith("batUpdate")) {
                  command = 12;
                } else if (inString.startsWith("{all}")) {
                  command = 13;
                } else {
                  command = 100;
                }
                while (all < numReadVars) {
                  switch (command) {
                    case 0:
                      Serial.println("");
                      Serial.print("Battery Voltage: ");
                      Serial.println(batteryVoltage);
                      Serial.print("Battery Percentage: ");
                      Serial.println(batteryLevel);
                      Serial.println("");
                      readCommand = false;
                      all++;
                      command = all;
                      inString = "";
                      break;
                    case 1:
                      Serial.println("");
                      for (uint8_t i = 0; i < 9; i++) {
                        Serial.print("ledValues[");
                        Serial.print(i);
                        Serial.print("]: ");
                        Serial.println(ledValues[i]);
                      }
                      Serial.println("");
                      readCommand = false;
                      all++;
                      command = all;
                      inString = "";
                      break;
                    case 2:
                      Serial.println("");
                      Serial.print("Analog Read A0: ");
                      Serial.println(analogRead(A0));
                      Serial.println("");
                      readCommand = false;
                      all++;
                      command = all;
                      inString = "";
                      break;
                    case 3:
                      Serial.println("");
                      Serial.println("         R, G, B");
                      for (uint8_t i = 0; i < 8; i++) {
                        Serial.print("leds[");
                        Serial.print(i);
                        Serial.print("]: ");
                        Serial.print(leds[i].r);
                        Serial.print(", ");
                        Serial.print(leds[i].g);
                        Serial.print(", ");
                        Serial.println(leds[i].b);
                      }
                      Serial.println("");
                      readCommand = false;
                      all++;
                      command = all;
                      inString = "";
                      break;
                    case 4:
                      Serial.println("");
                      Serial.print("Digital Read D3 (13): ");
                      Serial.println(digitalRead(usbPwr));
                      Serial.print("Charging: ");
                      Serial.println(charging);
                      Serial.println("");
                      readCommand = false;
                      all++;
                      command = all;
                      inString = "";
                      break;
                    case 5:
                      Serial.println("");
                      Serial.print("Battery Toggle: ");
                      Serial.println(lowBatToggle);
                      Serial.print("Charge Toggle: ");
                      Serial.println(chargeToggle);
                      Serial.println("");
                      readCommand = false;
                      all++;
                      command = all;
                      inString = "";
                      break;
                    case 6:
                      Serial.println("");
                      Serial.print("Screen Mode: ");
                      Serial.println(screenMode);
                      Serial.println("");
                      readCommand = false;
                      all++;
                      command = all;
                      inString = "";
                      break;
                    case 7:
                      Serial.println("");
                      Serial.print("Text Line 1: ");
                      Serial.println(text1);
                      Serial.print("Text Line 2: ");
                      Serial.println(text2);
                      Serial.print("Text Line 3: ");
                      Serial.println(text3);
                      Serial.println("");
                      readCommand = false;
                      all++;
                      command = all;
                      inString = "";
                      break;
                    case 8:
                      Serial.println("");
                      Serial.print("Screen Update: ");
                      Serial.println(screenUpdate);
                      Serial.println("");
                      readCommand = false;
                      all++;
                      command = all;
                      inString = "";
                      break;
                    case 9:
                      Serial.println("");
                      Serial.print("LED Update: ");
                      Serial.println(LEDupdate);
                      Serial.println("");
                      readCommand = false;
                      all++;
                      command = all;
                      inString = "";
                      break;
                    case 10:
                      Serial.println("");
                      Serial.print("LEDs On: ");
                      Serial.println(LEDsOn);
                      Serial.println("");
                      readCommand = false;
                      all++;
                      command = all;
                      inString = "";
                      break;
                    case 11:
                      Serial.println("");
                      for (uint8_t i = 0; i < 10; i++) {
                        Serial.print("Battery Voltage[");
                        Serial.print(i);
                        Serial.print("]: ");
                        Serial.println(batteryVoltages[i]);
                      }
                      Serial.print("Voltage Average: ");
                      Serial.println(batteryVoltage);
                      Serial.println("");
                      readCommand = false;
                      all++;
                      command = all;
                      inString = "";
                      break;
                    case 12:
                      Serial.println("");
                      Serial.print("Battery Update: ");
                      Serial.println(batteryUpdate);
                      Serial.println("");
                      readCommand = false;
                      all++;
                      command = all;
                      inString = "";
                      break;
                    case 13:
                      all = 0;
                      command = all;
                      break;
                    default:
                      Serial.println("");
                      Serial.println("Unknown variable to read.");
                      Serial.println("");
                      readCommand = false;
                      all = numReadVars + 1;
                      inString = "";
                      break;
                  }
                }
              }
            }
          }
          break;
        case 2:
          Serial.println("write not implemented yet...");
          Serial.println("you can implement it if you want you lazy fuck");
          Serial.println("");
          inString = "";
          break;
        case 3:
          server.stop();
          Serial.println("Rebooting WiFi...");
          Serial.println("");
          WiFi.mode(WIFI_OFF);
          initWifi();
          inString = "";
          break;
        default:
          Serial.print("unknown command... ");
          Serial.println(inString);
          Serial.println("");
          inString = "";
          break;
      }
    }
  }
}
