//
// Mr. Leigh Brady's Gift
//

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <FastLED.h>
#include <U8g2lib.h>
#include <Ticker.h>

// timer vars
Ticker nonWifiTimer;

// neopixel LED vars
#define NUM_LEDS 8
#define DATA_PIN 0
CRGB leds[NUM_LEDS];
// global brightness and first 8 led hues
// {bright, hue, hue, hue, hue, hue, hue, hue, hue}
uint8_t ledValues[] = {128, 0, 32, 64, 96, 128, 160, 192, 224};

// battery and charge vars
#define chargeLED 12
#define usbPwr 13
#define lowBatLED 14
#define lowBlink 3.7
#define lowSleep 3.5
float batteryVoltage = 4.2;
float batteryVoltages[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
uint8_t batteryLevel = 100;
volatile bool batteryUpdate = true;
volatile bool charging = false;
volatile bool lowBatToggle = true;
volatile bool chargeToggle = true;
volatile bool deepSleepToggle = false;

// serial interface vars
String inString = "";

// screen and text vars
U8G2_SH1106_128X64_NONAME_1_HW_I2C panel(U8G2_R0);
uint8_t screenMode = 3;
String text1 = " senior ";
String text2 = "electron";
String text3 = "wrangler";

// update vars
volatile bool screenUpdate = true;
volatile bool LEDupdate = false;
volatile bool LEDsOn = false;

// webserver vars
ESP8266WebServer server(80);

void setup(void) {
  initPins();
  initInterrupts();
  initPeripherals();
  Serial.println("booting...");
  initBattery();
  initWifi();
}

void loop(void) {
  delay(10);
  
  if (deepSleepToggle) {
    batDeepSleep();
  }

  server.handleClient();
 
  serialHandler();

  // disable interrupts for drawing screen and updating neopixel LEDSs
  noInterrupts();
  if (batteryUpdate) {
    updateBattery();
  }

  if (screenUpdate) {
    drawScreen(screenMode);
  }

  if (LEDupdate) {
    setLEDValues();
    FastLED.show();
    LEDupdate = false;
  }
  // enable interrupts after drawing screen and setting neopixel LEDs
  interrupts();
}
