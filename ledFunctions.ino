void setLEDValues() {
  for (uint8_t i = 0; i < 8; i++) {
    leds[i] = CHSV(ledValues[i + 1], 255, ledValues[0]);
  }
  LEDupdate = true;
}

void setAllLEDsOff() {
  fill_solid(leds, NUM_LEDS, CRGB(0, 0, 0));
  FastLED.show();
}

void redScan(uint8_t d) {
  setAllLEDsOff();
  for (uint8_t i = 0; i < 8; i++) {
    leds[i] = CRGB::Red;
    FastLED.show();
    delay(d);
    leds[i] = CRGB::Black;
    delay(d);
    FastLED.show();
  }
  for (uint8_t j = 8; j > 0; j--) {
    leds[j - 1] = CRGB::Red;
    FastLED.show();
    delay(d);
    leds[j - 1] = CRGB::Black;
    delay(d);
    FastLED.show();
  }
  if (LEDsOn) {
    setLEDValues();
  }
}

void greenScan(uint8_t d) {
  setAllLEDsOff();
  for (uint8_t i = 0; i < 8; i++) {
    leds[i] = CRGB::Green;
    FastLED.show();
    delay(d);
    leds[i] = CRGB::Black;
    delay(d);
    FastLED.show();
  }
  for (uint8_t j = 8; j > 0; j--) {
    leds[j - 1] = CRGB::Green;
    FastLED.show();
    delay(d);
    leds[j - 1] = CRGB::Black;
    delay(d);
    FastLED.show();
  }
  if (LEDsOn) {
    setLEDValues();
  }
}

void blueScan(uint8_t d) {
  setAllLEDsOff();
  for (uint8_t i = 0; i < 8; i++) {
    leds[i] = CRGB::Blue;
    FastLED.show();
    delay(d);
    leds[i] = CRGB::Black;
    delay(d);
    FastLED.show();
  }
  for (uint8_t j = 8; j > 0; j--) {
    leds[j - 1] = CRGB::Blue;
    FastLED.show();
    delay(d);
    leds[j - 1] = CRGB::Black;
    delay(d);
    FastLED.show();
  }
  if (LEDsOn) {
    setLEDValues();
  }
}
