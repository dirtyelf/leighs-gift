void handleRoot() {
  // this func handles the root (flange.local w/ MDNS or 192.168.4.1 raw ip)
  String website;
  String temp = "";
  String textWeb1 = "";
  String textWeb2 = "";
  String textWeb3 = "";
  bool newText = false;
  uint8_t numArgs = server.args();
  
  if (numArgs > 0) {
    // only change screen mode if needed
    // arg(0) = screen mode from radio boxes
    if (screenMode != server.arg(0).toInt()) {
      screenMode = server.arg(0).toInt();
      newText = true;
    }

    /*
      dont update the line if text box is left blank
      center the text for each line by adding spaces to the begining based on length
      arg(1) = line 1
      arg(2) = line 2
      arg(3) = line 3
    */
    if (server.arg(1) != "") {
      newText = true;
      switch (server.arg(1).length()) {
        case 6: case 5:
          text1 = " ";
          text1 += server.arg(1);
          break;
        case 4: case 3:
          text1 = "  ";
          text1 += server.arg(1);
          break;
        case 2: case 1:
          text1 = "   ";
          text1 += server.arg(1);
          break;
        default:
          text1 = server.arg(1);
          break;
      }
    }
    if (server.arg(2) != "") {
      newText = true;
      switch (server.arg(2).length()) {
        case 6: case 5:
          text2 = " ";
          text2 += server.arg(2);
          break;
        case 4: case 3:
          text2 = "  ";
          text2 += server.arg(2);
          break;
        case 2: case 1:
          text2 = "   ";
          text2 += server.arg(2);
          break;
        default:
          text2 = server.arg(2);
          break;
      }
    }
    if (server.arg(3) != "") {
      newText = true;
      switch (server.arg(3).length()) {
        case 6: case 5:
          text3 = " ";
          text3 += server.arg(3);
          break;
        case 4: case 3:
          text3 = "  ";
          text3 += server.arg(3);
          break;
        case 2: case 1:
          text3 = "   ";
          text3 += server.arg(3);
          break;
        default:
          text3 = server.arg(3);
          break;
      }
    }

    // redraw the screen if new text
    if (newText) {
      screenUpdate = true;
    }

    // set LED values based on inputs
    for (uint8_t i = 0; i < 8; i++) {
      ledValues[i + 1] = server.arg(i + 4).toInt();
    }
    ledValues[0] = server.arg(12).toInt();

    // if the LED on box is checked, set the LEDS to on
    // if box is not checked, turn them all off
    if (numArgs > 13 && server.arg(numArgs - 1) == "true") {
      LEDsOn = true;
      LEDupdate = true;
    } else {
      setAllLEDsOff();
      LEDsOn = false;
    }
  }

  website = "<!DOCTYPE html>";
  website += "<html>";
  website += "<head><title>Dirty Flange Net</title></head>";
  website += "<body>";
  website += "<h1>Welcome to Flange Net you dirty wanker!</h1>";
  website += "<form action='http://192.168.4.1' method='POST'>";
  website += "<p>Select display mode.</p>";
  switch (screenMode) {
    case 1:
      website += "Mode 1: <input type='radio' name='mode' value='1' checked><br>";
      website += "Mode 2: <input type='radio' name='mode' value='2'><br>";
      website += "Mode 3: <input type='radio' name='mode' value='3'><br>";
      break;
    case 2:
      website += "Mode 1: <input type='radio' name='mode' value='1'><br>";
      website += "Mode 2: <input type='radio' name='mode' value='2' checked><br>";
      website += "Mode 3: <input type='radio' name='mode' value='3'><br>";
      break;
    case 3:
      website += "Mode 1: <input type='radio' name='mode' value='1'><br>";
      website += "Mode 2: <input type='radio' name='mode' value='2'><br>";
      website += "Mode 3: <input type='radio' name='mode' value='3' checked><br>";
      break;
    // need a default with one of the options checked so that it sends correct
    // number of args every time
    default:
      website += "Mode 1: <input type='radio' name='mode' value='1' checked><br>";
      website += "Mode 2: <input type='radio' name='mode' value='2'><br>";
      website += "Mode 3: <input type='radio' name='mode' value='3'><br>";
      break;
  }
  website += "<p>Change your title here.</p> <p>Might I suggest lazy sod or wazzock?</p>";
  website += "<p>(leave blank for no change)</p>";
  website += "Line 1: <input type='text' name='lineone' maxlength='8' value='";

  temp = text1;
  text1.trim();
  textWeb1 = text1;
  text1 = temp;

  temp = text2;
  text2.trim();
  textWeb2 = text2;
  text2 = temp;

  temp = text3;
  text3.trim();
  textWeb3 = text3;
  text3 = temp;
  
  website += textWeb1;
  website += "'><br>";
  website += "Line 2: <input type='text' name='linetwo' maxlength='8' value='";
  website += textWeb2;
  website += "'><br>";
  website += "Line 3: <input type='text' name='linethr' maxlength='8' value='";
  website += textWeb3;
  website += "'><br>";
  website += "<p>Set the hue and brightness of the LEDs. Check the box for on, uncheck for off.</p>";
  website += "LED 1: <input type='range' name='ledone' min='0' max='255' value='";
  website += ledValues[1];
  website += "'><br>";
  website += "LED 2: <input type='range' name='ledtwo' min='0' max='255' value='";
  website += ledValues[2];
  website += "'><br>";
  website += "LED 3: <input type='range' name='ledthr' min='0' max='255' value='";
  website += ledValues[3];
  website += "'><br>";
  website += "LED 4: <input type='range' name='ledfou' min='0' max='255' value='";
  website += ledValues[4];
  website += "'><br>";
  website += "LED 5: <input type='range' name='ledfiv' min='0' max='255' value='";
  website += ledValues[5];
  website += "'><br>";
  website += "LED 6: <input type='range' name='ledsix' min='0' max='255' value='";
  website += ledValues[6];
  website += "'><br>";
  website += "LED 7: <input type='range' name='ledsev' min='0' max='255' value='";
  website += ledValues[7];
  website += "'><br>";
  website += "LED 8: <input type='range' name='ledeig' min='0' max='255' value='";
  website += ledValues[8];
  website += "'><br>";
  website += "Brightness: <input type='range' name='ledbri' min='25' max='255' value='";
  website += ledValues[0];
  website += "'><br>";
  website += "LEDs on: <input type='checkbox' name='ledson' value='true'";
  if (LEDsOn) {
    website += "checked";
  }
  website += "><br><br>";
  website += "<input type='submit' value='Piss off, bell end.'>";
  website += "</form><br>";
  website += "<p><a href='red' style='color: red;'>Red Scan</a>  ";
  website += "<a href='green' style='color: green;'>Green Scan</a>  ";
  website += "<a href='blue'>Blue Scan</a></p>";
  website += "<p><a href='all' style='color: black;'>RGB Scan</a></p><br>";
  website += "<p>Battery Level: ";
  website += batteryLevel;
  website += "% (";
  website += batteryVoltage;
  website += " volts)";
  website += "</p>";
  if (charging) {
    website += "<p>charging</p>";
  }
  website += "</body>";
  website += "</html>";

  server.sendHeader("Connection", "close");
  server.send(200, "text/html", website);

  delay(100);
 
  if (numArgs > 0) {
    // print all the args over serial
    Serial.print(numArgs);
    Serial.println(" arguments received from client.");
    for (uint8_t i = 0; i < 4; i++) {
      Serial.print(server.argName(i));
      Serial.print("\t");
      Serial.print("arg ");
      Serial.print(i);
      Serial.print("\t");
      Serial.println(server.arg(i));
    }
    for (uint8_t i = 4; i < server.args(); i++) {
      Serial.print(server.argName(i));
      Serial.print("\t");
      Serial.print("arg ");
      Serial.print(i);
      Serial.print("\t");
      if (i == 13) {
        Serial.println(server.arg(i));
      } else {
        Serial.println(server.arg(i).toInt());
      }
    }
    Serial.println(" ");
  }
}

void handleIO() {
  String website;

  website = "<html>";
  website += "<head><title>Analog Read</title></head>";
  website += "<body>";
  website += "<p>Analog Read Value: ";
  website += analogRead(A0);
  website += "</p>";
  website += "<p>Digital Read Value: ";
  website += digitalRead(usbPwr);
  website += "</p>";
  website += "</body>";
  website += "</html>";

  server.send(200, "text/html", website);
  delay(100);
}

void handleDraw() {
  String website;

  website = "<html>";
  website += "<head><title>Redraw</title></head>";
  website += "<body>";
  website += "<p>redrawing the screen...</p>";
  website += "</body>";
  website += "</html>";

  server.send(200, "text/html", website);
  delay(100);
  screenUpdate = true;
}

void handleRed() {
  redScan(25);
  server.sendHeader("Location", "/");
  server.send(303);
}

void handleGreen() {
  greenScan(25);
  server.sendHeader("Location", "/");
  server.send(303);
}

void handleBlue() {
  blueScan(25);
  server.sendHeader("Location", "/");
  server.send(303);
}

void handleAll() {
  redScan(8);
  greenScan(8);
  blueScan(8);
  server.sendHeader("Location", "/");
  server.send(303);
}
