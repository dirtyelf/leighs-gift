void updateBattery() {
  // every two seconds this is called from interrupt
  // check the battery level and update the chrg/low bat leds
  float rawRead = analogRead(A0) / (float)1023;
  float instantVoltage = rawRead * 4.2;

  // fill array with latest value
  for (uint8_t i = 0; i < 10; i++) {
    if (i == 9) {
      batteryVoltages[i] = instantVoltage;
    } else {
      batteryVoltages[i] = batteryVoltages[i + 1];
    }
  }

  // fill batteryVoltage with average
  // if there is still a 0 in array use instantaneous value
  if (avgBatVolt(batteryVoltages) != 0) {
    batteryVoltage = avgBatVolt(batteryVoltages);
  } else {
    batteryVoltage = instantVoltage;
  }

  // fill batteryLevel with percentage based on battery voltage
  batteryLevel = batteryPercent(batteryVoltage);
  // done updating battery until next interrupt
  batteryUpdate = false;
}

uint8_t batteryPercent(float batVolts) {
  // estimate battery percentage based on custom formulas
  // split the non-linear data into three linear sections
  // battery of unknow pedigree, estimate 2500 mAh remaining
  float percentageRemaining;
  float mAhTotal = 2500;
  float x;
  float c;
  if (batVolts > 4) {
    x = -0.001;
    c = 4.1898;
    percentageRemaining = (mAhTotal - ((batVolts - c) / x)) / mAhTotal;
  } else if (batVolts > 3.6) {
    x = -0.00019;
    c = 4.035;
    percentageRemaining = (mAhTotal - ((batVolts - c) / x)) / mAhTotal;
  } else if (batVolts > 3.2) {
    x = -0.00232;
    c = 8.9615;
    percentageRemaining = (mAhTotal - ((batVolts - c) / x)) / mAhTotal;
  } else {
    Serial.println("bat percent else clause returned 0");
    return 0;
  }
  if (percentageRemaining < 0) {
    percentageRemaining = 0;
  } else if (percentageRemaining > 1) {
    percentageRemaining = 1;
  }
  percentageRemaining *= 100;
  return (uint8_t)percentageRemaining;
}

float avgBatVolt(float voltages[]) {
  static uint8_t numElements = 10;
  uint8_t count = 0;
  float total = 0;
  for (uint8_t i = 0; i < numElements; i++) {
    total += batteryVoltages[i];
    if (batteryVoltages[i] != 0) {
      count++;
    }
  }
  if (count == numElements) {
    return total / (float)numElements;
  } else {
    return 0;
  }
}

void batDeepSleep() {
  digitalWrite(chargeLED, LOW);
  digitalWrite(lowBatLED, LOW);
  fill_solid(leds, 8, CRGB(0, 0, 0));
  panel.clear();
  panel.firstPage();
  do {
    panel.setFont(u8g2_font_open_iconic_embedded_2x_t);
    panel.drawGlyph(64, 32, 0x40); // empty glyph
  } while (panel.nextPage());
  Serial.println("low battery... sleeping.");
  ESP.deepSleep(30 * 1000000); // sleep for 30 seconds
  delay(10);
  // while loop takes the place of the deep sleep for debugging
  /*
  while (batteryVoltage < lowBlink) {
    Serial.println("deep sleep...");
    Serial.print("battery below 3.7V (");
    Serial.print(batteryVoltage);
    Serial.println(" volts)");
    updateBattery();
    Serial.print("battery updated (");
    Serial.print(batteryVoltage);
    Serial.println(" volts)");
    delay(2000);
  }
  deepSleepToggle = false;
  */
}
