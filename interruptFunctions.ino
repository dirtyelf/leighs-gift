/*
   keep these short and sweet
*/

void twoSecISR() {
  if (batteryVoltage < lowBlink) {
    if (batteryVoltage < lowSleep) {
      deepSleepToggle = true;
      return;
    }
    lowBatToggle = !lowBatToggle;
    digitalWrite(lowBatLED, lowBatToggle);
  } else {
    digitalWrite(lowBatLED, LOW);
  }
  if (charging && batteryLevel == 100) {
    digitalWrite(chargeLED, HIGH);
  } else if (charging) {
    chargeToggle = !chargeToggle;
    digitalWrite(chargeLED, chargeToggle);
  } else {
    digitalWrite(chargeLED, LOW);
  }
  batteryUpdate = true;
  screenUpdate = true;
}

void chrgPinISR() {
  charging = digitalRead(usbPwr);
  batteryUpdate = true;
  screenUpdate = true;
}
